#!/usr/bin/env python3

import tkinter as tk
import configparser
import gui

if __name__ == "__main__":
    config = configparser.ConfigParser()
    try:
        config.read("settings.ini")
        jsonPath = config["DEFAULT"]["DEVICE"]
    except configparser.Error as err:
        print(err)

    root = tk.Tk()
    main = gui.mainWindow(parent=root, jsonPath=jsonPath)
    root.mainloop()
