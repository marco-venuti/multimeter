import tkinter as tk
import json
import numpy as np


def propagateError(value, read, digit):
    return np.sqrt((value*read)**2 + digit**2)


class mainWindow(tk.Frame):
    def __init__(self, parent, jsonPath):
        super().__init__(parent)
        self.parent = parent
        parent.title("Incertezze multimetro")
        parent.resizable(False, False)
        self.choice = tk.IntVar()
        self.choice.set(0)
        self.grid(padx=20, pady=20)
        tk.Label(self, text="Grandezza", justify=tk.LEFT,
                 padx=20).grid(row=0, column=0)

        self.loadDevice(jsonPath)
        self.modes = self.device["modes"]

        for mode in enumerate(self.modes):
            tk.Radiobutton(self,
                           text=mode[1]["label"],
                           padx=20,
                           variable=self.choice,
                           value=mode[0]).grid(row=mode[0]+1, column=0, sticky=tk.W)

        tk.Label(self, text="Misura").grid(row=0, column=1)
        self.measure = tk.Entry(self)
        self.measure.grid(row=1, column=1)
        tk.Button(self, text="Calcola", command=self.compute).grid(
            row=3, column=1)
        tk.Label(self, text="Incertezza:").grid(row=5, column=1)
        self.output = tk.Label(self)
        self.output.grid(row=6, column=1)

    def loadDevice(self, jsonPath):
        with open(jsonPath) as jsonFile:
            self.device = json.load(jsonFile)

    def compute(self):
        try:
            measure = float(self.measure.get())
        except ValueError:
            self.output["text"] = "Valore non valido!"
            return
        modeRange = self.modes[self.choice.get()]["range"]

        for i in modeRange:
            if i["value"] > measure:
                curRange = i
                break
        if "curRange" not in locals():
            raise("Fuori scala!")

        error = propagateError(measure, float(
            curRange["read"]), float(curRange["digit"]))

        self.output["text"] = ('%.3g' % error)
